# NLP Prosa Test answer
 
AI (NLP) Engineer technical test answer from Prosa. This repository provide a notebook that conclude the experiments have been conducted and a python script that can be used for testing the model.

## The experiment
In this experiment, I did an two phases of modelling. The first modeling is to train a GloVe vector, and  using GloVe framework 

### GloVe
Instalation of GloVe that can be found [here](https://github.com/stanfordnlp/GloVe). I used wikipedia articles to the year of 2020. GloVe is used to change word representation before we feed the data into our model for training. We train GloVe with five milion words, more or less. 

### Deep Neural Network Model
I make FastText model with only have one embedding layer and one hidden layer. This model provide better training and inference time with not so big different impact of accuracy. 
I use pytorch and, a new pytorch for text processing utilities, torchtext. For embedding layer, we include the vector from GloVe we train earlier as the weight for that layer. 


## How to run
To run the model, please first install the python requirement that have been listed in `requirement` file. After the installation, run the `test.py` from the terminal.
```sh
python test.py 
```
or if you want to use other text, please use the command below.
```sh
python test.py <teks>
```